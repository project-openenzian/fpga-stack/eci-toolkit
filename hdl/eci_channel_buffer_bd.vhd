-------------------------------------------------------------------------------
-- eci_channel_buffer - BD wrapper - Behavioral
-------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------
-- ECI Channel buffer/register
-- FULL - decouple valid/ready signals if true, otherwise couple valid signals (no latency)
-- COALESCE - in case of two-beat messages (13 or 17 words), don't send the 1st part of a message until the 2nd part arrives

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity eci_channel_buffer_bd is
generic (
    FULL        : boolean := true;
    COALESCE    : boolean := false -- coalesce two beats of a packet
);
port (
    clk             : in STD_LOGIC;

    input0_header   : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data0    : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data1    : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data2    : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data3    : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data4    : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data5    : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data6    : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data7    : in std_logic_vector(63 downto 0) := (others => '0');
    input0_vc_no    : in std_logic_vector(3 downto 0) := (others => '0');
    input0_size     : in std_logic_vector(2 downto 0) := (others => '0');
    input0_valid    : in std_logic := '0';
    input0_ready    : out std_logic;

    output0_header  : out std_logic_vector(63 downto 0);
    output0_data0   : out std_logic_vector(63 downto 0);
    output0_data1   : out std_logic_vector(63 downto 0);
    output0_data2   : out std_logic_vector(63 downto 0);
    output0_data3   : out std_logic_vector(63 downto 0);
    output0_data4   : out std_logic_vector(63 downto 0);
    output0_data5   : out std_logic_vector(63 downto 0);
    output0_data6   : out std_logic_vector(63 downto 0);
    output0_data7   : out std_logic_vector(63 downto 0);
    output0_vc_no   : out std_logic_vector(3 downto 0);
    output0_size    : out std_logic_vector(2 downto 0);
    output0_valid   : out std_logic;
    output0_ready   : in  std_logic
);
end eci_channel_buffer_bd;

architecture Behavioral of eci_channel_buffer_bd is

signal input0, output0 : ECI_CHANNEL;

begin

output0_header <= output0.data(0);
output0_data0 <= output0.data(1);
output0_data1 <= output0.data(2);
output0_data2 <= output0.data(3);
output0_data3 <= output0.data(4);
output0_data4 <= output0.data(5);
output0_data5 <= output0.data(6);
output0_data6 <= output0.data(7);
output0_data7 <= output0.data(8);
output0_vc_no <= output0.vc_no;
output0_size <= output0.size;
output0_valid <= output0.valid;

input0.data(0) <= input0_header;
input0.data(1) <= input0_data0;
input0.data(2) <= input0_data1;
input0.data(3) <= input0_data2;
input0.data(4) <= input0_data3;
input0.data(5) <= input0_data4;
input0.data(6) <= input0_data5;
input0.data(7) <= input0_data6;
input0.data(8) <= input0_data7;
input0.vc_no <= input0_vc_no;
input0.size <= input0_size;
input0.valid <= input0_valid;

i_buffer : entity work.eci_channel_buffer
generic map (
    FULL => FULL,
    COALESCE => COALESCE
)
port map (
    clk => clk,
    input           => input0,
    input_ready     => input0_ready,
    output          => output0,
    output_ready    => output0_ready
);

end Behavioral;
