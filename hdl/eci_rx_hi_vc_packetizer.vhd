----------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------
-- Find ECI packets in a stream of ECI words
-- Take up to 3 words, look for a header, find a whole packet and send it

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity eci_rx_hi_vc_packetizer is
generic (
    VC_NO           : integer
);
port (
    clk             : in std_logic;

    input_data      : in WORDS(2 downto 0);             -- stream of words, up to 3
    input_size      : in std_logic_vector(1 downto 0);  -- number of words
    input_length    : in std_logic_vector(14 downto 0); -- 3x5 bits describing length of a potential message, taken from the dmask field
    input_valid     : in std_logic;
    input_ready     : out std_logic;

    output          : buffer ECI_CHANNEL;
    output_ready    : in std_logic
);
end eci_rx_hi_vc_packetizer;

architecture Behavioral of eci_rx_hi_vc_packetizer is

signal phase            : integer range 0 to 5:= 0;
signal last_beat        : boolean;
signal last_beat2        : std_logic;
signal buf              : WORDS(7 downto 0);
signal msg_pos          : integer range 0 to 8 := 0;

signal beat_valid       : std_logic;
signal beat_size        : integer range 1 to 9;
signal aligned_size     : integer range 0 to 3;

-- ECI message length in words
signal msg_length       : integer range 0 to 31;
signal msg_length_buf   : integer range 0 to 31;

begin

msg_length  <= msg_length_buf when msg_pos /= 0 else to_integer(unsigned(input_length(4 downto 0)));
aligned_size <= to_integer(unsigned(input_size)) when input_valid = '1' else 0;
beat_size   <=
--    2 when (phase = 0 and (msg_length >= 2 and msg_length <= 4)) else
    5 when (phase = 0 and (msg_length >= 5 and msg_length <= 8)) else
    9 when (phase = 0 and msg_length >= 9) else
    5 when (phase = 1 and (msg_length >= 13 and msg_length <= 16)) else
    9 when (phase = 1 and (msg_length >= 17 or msg_length <= 20)) else
    2;
beat_valid  <= '1' when aligned_size + msg_pos >= beat_size else '0';
last_beat <= true when
    (phase = 0 and (msg_length = 2 or msg_length = 5 or msg_length = 9)) or
    (phase = 1 and (msg_length = 3 or msg_length = 6 or msg_length = 10 or msg_length = 13 or msg_length = 17)) or
    (phase = 2 and (msg_length = 4 or msg_length = 7 or msg_length = 11 or msg_length = 14 or msg_length = 18)) or
    (phase = 3 and (msg_length = 8 or msg_length = 12 or msg_length = 15 or msg_length = 19)) or
    (phase = 4 and (msg_length = 16 or msg_length = 20)) else false;
last_beat2 <= to_std_logic(last_beat); 
input_ready <= '1' when beat_valid = '0' or (beat_valid = '1' and output_ready = '1') else '0';

-- Possible combinations:
-- msg_length   phases 0  1  2  3  4
-- 1        header+0   0L <- doesn't appear in this VC
-- 2                1  1L
-- 3                2  1F 1L
-- 4                3  1F 1F 1L
-- 5                4  4L
-- 6                5  4F 1L
-- 7                6  4F 1F 1L
-- 8                7  4F 1F 1F 1L
-- 9                8  8L
-- 10               9  8F 1L
-- 11               10 8F 1F 1L
-- 12               11 8F 1F 1F 1L
-- 13               12 8F 4L
-- 14               13 8F 4F 1L
-- 15               14 8F 4F 1F 1L
-- 16               15 8F 4F 1F 1F 1L
-- 17               16 8F 8L
-- 18               17 8F 8F 1L
-- 19               18 8F 8F 1F 1L
-- 20               19 8F 8F 1F 1F 1L

-- beat size
--      first   next
-- 0L   1
-- 1L   2       1
-- 4L   5       4
-- 8L   9       8
-- 1F   2       1
-- 4F   5       4
-- 8F   9       8


output.data(0)  <= buf(0);
output.data(1)  <= input_data(0) when msg_pos = 1 else buf(1);
output.data(2)  <= input_data(0) when msg_pos = 2 else buf(2);
output.data(3)  <= input_data(0) when msg_pos = 3 else input_data(1) when msg_pos = 2 else buf(3);
output.data(4)  <= input_data(0) when msg_pos = 4 else input_data(1) when msg_pos = 3 else input_data(2) when msg_pos = 2 else buf(4);
output.data(5)  <= buf(5);
output.data(6)  <= input_data(0) when msg_pos = 6 else buf(6);
output.data(7)  <= input_data(0) when msg_pos = 7 else input_data(1) when msg_pos = 6 else buf(7);
output.data(8)  <= input_data(0) when msg_pos = 8 else input_data(1) when msg_pos = 7 else input_data(2);

output.size <=
    ECI_CHANNEL_SIZE_1F when not last_beat and (beat_size = 1 or beat_size = 2) else
    ECI_CHANNEL_SIZE_1L when last_beat and (beat_size = 1 or beat_size = 2) else
    ECI_CHANNEL_SIZE_4F when not last_beat and (beat_size = 4 or beat_size = 5) else
    ECI_CHANNEL_SIZE_4L when last_beat and (beat_size = 4 or beat_size = 5) else
    ECI_CHANNEL_SIZE_8F when not last_beat and (beat_size = 8 or beat_size = 9) else
    ECI_CHANNEL_SIZE_8L;-- when last_beat and (beat_size = 8 or beat_size = 9);

output.vc_no    <= std_logic_vector(to_unsigned(VC_NO, 4));
output.valid    <= beat_valid;

i_process : process(clk)
    variable i, o       : integer;
    variable buf_copy_start : integer range 0 to 2;
    variable buf_copy_count : integer range 0 to 3;
    variable buf_copy_where : integer range 0 to 8;
begin
    if rising_edge(clk) then
        if beat_valid = '1' and output_ready = '1' then
            if not last_beat then
                phase <= phase + 1;
            else
                phase <= 0;
            end if;
        end if;

        if beat_valid = '0' or output_ready = '1' then
            if aligned_size + msg_pos < beat_size then  -- not enough data for a beat, copy all
                buf_copy_start := 0;
                buf_copy_count := aligned_size;
            else                                        -- enough data for a beat, copy the rest
                buf_copy_start := beat_size - msg_pos;
                buf_copy_count := aligned_size + msg_pos - beat_size;
            end if;

            if beat_valid = '0' then -- append
                buf_copy_where := msg_pos;
            elsif not is_eci_channel_cycle_last(output.size) then -- 1st beat was sent, keep the header
                buf_copy_where := 1;
            else -- whole packet was sent, start from 0
                buf_copy_where := 0;
            end if;

            for i in 0 to 2 loop
                if i < buf_copy_count then
                    buf(buf_copy_where + i) <= input_data(buf_copy_start + i);
                end if;
            end loop;

            if buf_copy_count /= 0 and buf_copy_where = 0 then
                msg_length_buf <= to_integer(unsigned(input_length(5*buf_copy_start+4 downto 5*buf_copy_start)));
            end if;

            msg_pos <= buf_copy_where + buf_copy_count;
        end if;
    end if;
end process;

end Behavioral;
