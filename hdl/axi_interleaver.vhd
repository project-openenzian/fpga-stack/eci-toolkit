-------------------------------------------------------------------------------
-- Module Name: AXI interleaver - Behavioral
-------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------
-- Interleave, convert bursts into single transactions
-- Up to 256 burst size x 1024-bit wide
-- Split into 2 burst x 512-bit wide
-- The master channel is chosen based on the aliased address
-- So it can be connected to the proper ECI module

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity axi_interleaver is
generic (
    ID_WIDTH : integer range 0 to 19 := 0
);
port (
    clk : in std_logic;
    resetn : in std_logic;
-- app AXI
    s_axi_awaddr : in std_logic_vector(39 downto 0);
    s_axi_awid      : in std_logic_vector(ID_WIDTH downto 0);
    s_axi_awlen : in std_logic_vector(7 downto 0);
    s_axi_awsize : in std_logic_vector(2 downto 0);
    s_axi_awburst : in std_logic_vector(1 downto 0);
    s_axi_awvalid : in std_logic;
    s_axi_awready : out std_logic := '0';
    
    s_axi_wdata : in std_logic_vector(1023 downto 0);
    s_axi_wstrb : in std_logic_vector(127 downto 0);
    s_axi_wlast : in std_logic;
    s_axi_wvalid : in std_logic;
    s_axi_wready : out std_logic := '0';
    
    s_axi_bready : in std_logic;
    s_axi_bid      : out std_logic_vector(ID_WIDTH downto 0);
    s_axi_bresp : out std_logic_vector(1 downto 0);
    s_axi_bvalid : out std_logic := '0';
    
    s_axi_araddr : in std_logic_vector(39 downto 0);
    s_axi_arid      : in std_logic_vector(ID_WIDTH downto 0);
    s_axi_arlen : in std_logic_vector(7 downto 0);
    s_axi_arsize : in std_logic_vector(2 downto 0);
    s_axi_arburst : in std_logic_vector(1 downto 0);
    s_axi_arvalid : in std_logic;
    s_axi_arready : out std_logic := '0';
    
    s_axi_rready : in std_logic;
    s_axi_rid      : out std_logic_vector(ID_WIDTH downto 0);
    s_axi_rlast : out std_logic := '0';
    s_axi_rvalid : out std_logic := '0';
    s_axi_rresp : out std_logic_vector(1 downto 0);
    s_axi_rdata : out std_logic_vector(1023 downto 0);
-- master 0
    m0_axi_awaddr : out std_logic_vector(39 downto 0);
    m0_axi_awlen : out std_logic_vector(7 downto 0);
    m0_axi_awsize : out std_logic_vector(2 downto 0);
    m0_axi_awburst : out std_logic_vector(1 downto 0);
    m0_axi_awid : out std_logic_vector(3 downto 0);
    m0_axi_awvalid : out std_logic;
    m0_axi_awready : in std_logic;
    
    m0_axi_wdata : out std_logic_vector(511 downto 0);
    m0_axi_wstrb : out std_logic_vector(63 downto 0);
    m0_axi_wlast : out std_logic;
    m0_axi_wvalid : out std_logic;
    m0_axi_wready : in std_logic;
    
    m0_axi_bresp : in std_logic_vector(1 downto 0);
    m0_axi_bid : in std_logic_vector(3 downto 0);
    m0_axi_bvalid : in std_logic;
    m0_axi_bready : out std_logic;
    
    m0_axi_araddr : out std_logic_vector(39 downto 0);
    m0_axi_arlen : out std_logic_vector(7 downto 0);
    m0_axi_arsize : out std_logic_vector(2 downto 0);
    m0_axi_arburst : out std_logic_vector(1 downto 0);
    m0_axi_arid : out std_logic_vector(3 downto 0);
    m0_axi_arvalid : out std_logic;
    m0_axi_arready : in std_logic;
    
    m0_axi_rlast : in std_logic;
    m0_axi_rresp : in std_logic_vector(1 downto 0);
    m0_axi_rdata : in std_logic_vector(511 downto 0);
    m0_axi_rid : in std_logic_vector(3 downto 0);
    m0_axi_rvalid : in std_logic;
    m0_axi_rready : out std_logic;
-- master 1
    m1_axi_awaddr : out std_logic_vector(39 downto 0);
    m1_axi_awlen : out std_logic_vector(7 downto 0);
    m1_axi_awsize : out std_logic_vector(2 downto 0);
    m1_axi_awburst : out std_logic_vector(1 downto 0);
    m1_axi_awid : out std_logic_vector(3 downto 0);
    m1_axi_awvalid : out std_logic;
    m1_axi_awready : in std_logic;
    
    m1_axi_wdata : out std_logic_vector(511 downto 0);
    m1_axi_wstrb : out std_logic_vector(63 downto 0);
    m1_axi_wlast : out std_logic;
    m1_axi_wvalid : out std_logic;
    m1_axi_wready : in std_logic;
    
    m1_axi_bresp : in std_logic_vector(1 downto 0);
    m1_axi_bid : in std_logic_vector(3 downto 0);
    m1_axi_bvalid : in std_logic;
    m1_axi_bready : out std_logic;
    
    m1_axi_araddr : out std_logic_vector(39 downto 0);
    m1_axi_arlen : out std_logic_vector(7 downto 0);
    m1_axi_arsize : out std_logic_vector(2 downto 0);
    m1_axi_arburst : out std_logic_vector(1 downto 0);
    m1_axi_arid : out std_logic_vector(3 downto 0);
    m1_axi_arvalid : out std_logic;
    m1_axi_arready : in std_logic;
    
    m1_axi_rlast : in std_logic;
    m1_axi_rresp : in std_logic_vector(1 downto 0);
    m1_axi_rdata : in std_logic_vector(511 downto 0);
    m1_axi_rid : in std_logic_vector(3 downto 0);
    m1_axi_rvalid : in std_logic;
    m1_axi_rready : out std_logic;
-- master 2
    m2_axi_awaddr : out std_logic_vector(39 downto 0);
    m2_axi_awlen : out std_logic_vector(7 downto 0);
    m2_axi_awsize : out std_logic_vector(2 downto 0);
    m2_axi_awburst : out std_logic_vector(1 downto 0);
    m2_axi_awid : out std_logic_vector(3 downto 0);
    m2_axi_awvalid : out std_logic;
    m2_axi_awready : in std_logic;
    
    m2_axi_wdata : out std_logic_vector(511 downto 0);
    m2_axi_wstrb : out std_logic_vector(63 downto 0);
    m2_axi_wlast : out std_logic;
    m2_axi_wvalid : out std_logic;
    m2_axi_wready : in std_logic;
    
    m2_axi_bresp : in std_logic_vector(1 downto 0);
    m2_axi_bid : in std_logic_vector(3 downto 0);
    m2_axi_bvalid : in std_logic;
    m2_axi_bready : out std_logic;
    
    m2_axi_araddr : out std_logic_vector(39 downto 0);
    m2_axi_arlen : out std_logic_vector(7 downto 0);
    m2_axi_arsize : out std_logic_vector(2 downto 0);
    m2_axi_arburst : out std_logic_vector(1 downto 0);
    m2_axi_arid : out std_logic_vector(3 downto 0);
    m2_axi_arvalid : out std_logic;
    m2_axi_arready : in std_logic;
    
    m2_axi_rlast : in std_logic;
    m2_axi_rresp : in std_logic_vector(1 downto 0);
    m2_axi_rdata : in std_logic_vector(511 downto 0);
    m2_axi_rid : in std_logic_vector(3 downto 0);
    m2_axi_rvalid : in std_logic;
    m2_axi_rready : out std_logic;
-- master 3
    m3_axi_awaddr : out std_logic_vector(39 downto 0);
    m3_axi_awlen : out std_logic_vector(7 downto 0);
    m3_axi_awsize : out std_logic_vector(2 downto 0);
    m3_axi_awburst : out std_logic_vector(1 downto 0);
    m3_axi_awid : out std_logic_vector(3 downto 0);
    m3_axi_awvalid : out std_logic;
    m3_axi_awready : in std_logic;
    
    m3_axi_wdata : out std_logic_vector(511 downto 0);
    m3_axi_wstrb : out std_logic_vector(63 downto 0);
    m3_axi_wlast : out std_logic;
    m3_axi_wvalid : out std_logic;
    m3_axi_wready : in std_logic;
    
    m3_axi_bresp : in std_logic_vector(1 downto 0);
    m3_axi_bid : in std_logic_vector(3 downto 0);
    m3_axi_bvalid : in std_logic;
    m3_axi_bready : out std_logic;
    
    m3_axi_araddr : out std_logic_vector(39 downto 0);
    m3_axi_arlen : out std_logic_vector(7 downto 0);
    m3_axi_arsize : out std_logic_vector(2 downto 0);
    m3_axi_arburst : out std_logic_vector(1 downto 0);
    m3_axi_arid : out std_logic_vector(3 downto 0);
    m3_axi_arvalid : out std_logic;
    m3_axi_arready : in std_logic;
    
    m3_axi_rlast : in std_logic;
    m3_axi_rresp : in std_logic_vector(1 downto 0);
    m3_axi_rdata : in std_logic_vector(511 downto 0);
    m3_axi_rid : in std_logic_vector(3 downto 0);
    m3_axi_rvalid : in std_logic;
    m3_axi_rready : out std_logic
);
end axi_interleaver;

architecture Behavioral of axi_interleaver is

ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
ATTRIBUTE X_INTERFACE_PARAMETER of s_axi_araddr: SIGNAL is "CLK_DOMAIN clk,MAX_BURST_LENGTH 256,NUM_WRITE_OUTSTANDING 1,NUM_READ_OUTSTANDING 1,SUPPORTS_NARROW_BURST 0";
ATTRIBUTE X_INTERFACE_PARAMETER of m0_axi_araddr: SIGNAL is "CLK_DOMAIN clk,MAX_BURST_LENGTH 2,NUM_WRITE_OUTSTANDING 16,NUM_READ_OUTSTANDING 16,SUPPORTS_NARROW_BURST 0";
ATTRIBUTE X_INTERFACE_PARAMETER of m1_axi_araddr: SIGNAL is "CLK_DOMAIN clk,MAX_BURST_LENGTH 2,NUM_WRITE_OUTSTANDING 16,NUM_READ_OUTSTANDING 16,SUPPORTS_NARROW_BURST 0";
ATTRIBUTE X_INTERFACE_PARAMETER of m2_axi_araddr: SIGNAL is "CLK_DOMAIN clk,MAX_BURST_LENGTH 2,NUM_WRITE_OUTSTANDING 16,NUM_READ_OUTSTANDING 16,SUPPORTS_NARROW_BURST 0";
ATTRIBUTE X_INTERFACE_PARAMETER of m3_axi_araddr: SIGNAL is "CLK_DOMAIN clk,MAX_BURST_LENGTH 2,NUM_WRITE_OUTSTANDING 16,NUM_READ_OUTSTANDING 16,SUPPORTS_NARROW_BURST 0";

type m_w is record
    awaddr : std_logic_vector(39 downto 0);
    awlen : std_logic_vector(7 downto 0);
    awsize : std_logic_vector(2 downto 0);
    awburst : std_logic_vector(1 downto 0);
    awid : std_logic_vector(3 downto 0);
    awvalid : std_logic;
    
    wdata : std_logic_vector(511 downto 0);
    wdata2 : std_logic_vector(511 downto 0);
    wstrb : std_logic_vector(63 downto 0);
    wstrb2 : std_logic_vector(63 downto 0);
    wlast : std_logic;
    wvalid : std_logic;
    
    bready : std_logic;
    
    araddr : std_logic_vector(39 downto 0);
    arlen : std_logic_vector(7 downto 0);
    arsize : std_logic_vector(2 downto 0);
    arburst : std_logic_vector(1 downto 0);
    arid : std_logic_vector(3 downto 0);
    arvalid : std_logic;
    
    rready : std_logic;
    rdata : std_logic_vector(1023 downto 0);
    rid : integer;--std_logic_vector(3 downto 0);
    rvalid : std_logic;
end record;

type array_m_w is array (integer range <>) of m_w;

type m_wi is record
    awready : std_logic;
    wready : std_logic;
    bresp : std_logic_vector(1 downto 0);
    bid : std_logic_vector(3 downto 0);
    bvalid : std_logic;
    arready : std_logic;
    rlast : std_logic;
    rresp : std_logic_vector(1 downto 0);
    rdata : std_logic_vector(511 downto 0);
    rid : std_logic_vector(3 downto 0);
    rvalid : std_logic;
end record;

type array_m_wi is array (integer range <>) of m_wi;

constant m_w_zero : m_w := (
    awvalid => '0',
    awaddr => (others => '0'),
    awlen => (others => '0'),
    awsize => (others => '0'),
    awburst => (others => '0'),
    awid => (others => '0'),
    wvalid => '0',
    wdata => (others => '0'),
    wdata2 => (others => '0'),
    wstrb => (others => '0'),
    wstrb2 => (others => '0'),
    wlast => '0',
    bready => '1',
    arvalid => '0',
    araddr => (others => '0'),
    arlen => (others => '0'),
    arsize => (others => '0'),
    arburst => (others => '0'),
    arid => (others => '0'),
    rready => '1',
    rdata => (others => '0'),
    rid => 0,
    rvalid => '0'
);

-- Master AXI ports
signal m_ws : array_m_w(3 downto 0) := (others => m_w_zero);
signal m_wis : array_m_wi(3 downto 0);

signal m_awactive : boolean := false;
signal m_awaddr : std_logic_vector(39 downto 0);
signal m_awid : std_logic_vector(ID_WIDTH downto 0);
signal m_awcounter : integer := 0;

signal m_aractive : boolean := false;
signal m_araddr : std_logic_vector(39 downto 0);
signal m_arid : std_logic_vector(ID_WIDTH downto 0);
signal m_arcounter : integer := 0;

type cacheline is array (integer range <>) of std_logic_vector(1023 downto 0);

signal m_rbuffer : cacheline(255 downto 0);
signal m_rbuffer_valid : std_logic_vector(255 downto 0); -- bitmask of valid cachelines in the buffer

signal m_rbuffer_data : std_logic_vector(1023 downto 0);    -- current read out cacheline
signal m_rbuffer_data_valid : std_logic := '0';
signal m_rbuffer_addr_read : integer;
signal m_rbuffer_raddr : integer := 0;                      -- points to m_rsent_no + 2

signal m_rbuffer_data1 : std_logic_vector(1023 downto 0);   -- read out cacheline m_rsent_no+1
signal m_rbuffer_data1_no : integer;
signal m_rbuffer_data1_valid : std_logic := '0';

signal m_rbuffer_data2 : std_logic_vector(1023 downto 0);   -- read out cacheline m_rsent_no+1
signal m_rbuffer_data2_no : integer;
signal m_rbuffer_data2_valid : std_logic := '0';

signal m_raddr : std_logic_vector(39 downto 0);
signal m_rlen : integer range 0 to 255;
signal m_rbusy : std_logic := '0';
signal m_rid : integer;

signal m_rsent_no : integer;
signal m_rsent_no_p1 : integer;
signal m_rsent_no_p2 : integer;

signal incoming_data : std_logic_vector(1023 downto 0);
signal incoming_no : integer;
signal incoming_valid : boolean;

signal s_axi_rvalid_b : std_logic;

type id_array is array (integer range <>, integer range <>) of integer range 0 to 255;
signal ids : id_array(3 downto 0, 15 downto 0); -- 4 channels x 16 ids x 8 bits -> 64 total ids
type id_mask_array is array (integer range <>) of std_logic_vector(15 downto 0);
signal id_mask : id_mask_array(3 downto 0) := (others => (others => '0'));

-- 00 -> odd VCs
-- 01 -> even VCs
-- 10 -> odd VCs
-- 11 -> even VCs
function get_channel(X : std_logic_vector) return integer is
    variable aliased : std_logic_vector(32 downto 0);
begin
    aliased := eci_alias_cache_line_index(X(39 downto 7));
    return to_integer(unsigned(aliased(1 downto 0)));
end function;

impure function get_next_free(ch : integer) return integer is
    variable i : integer;
begin
    for i in 0 to 15 loop
        if id_mask(ch)(i) = '0' then
            return i;
        end if;
    end loop;
    return -1;
end function;

begin

m0_axi_awvalid <= m_ws(0).awvalid;
m0_axi_awaddr <= m_ws(0).awaddr;
m0_axi_awlen <= m_ws(0).awlen;
m0_axi_awsize <= m_ws(0).awsize;
m0_axi_awburst <= m_ws(0).awburst;
m0_axi_awid <= m_ws(0).awid;
m0_axi_wvalid <= m_ws(0).wvalid;
m0_axi_wdata <= m_ws(0).wdata;
m0_axi_wstrb <= m_ws(0).wstrb;
m0_axi_wlast <= m_ws(0).wlast;
m0_axi_bready <= m_ws(0).bready;
m0_axi_arvalid <= m_ws(0).arvalid;
m0_axi_araddr <= m_ws(0).araddr;
m0_axi_arlen <= m_ws(0).arlen;
m0_axi_arsize <= m_ws(0).arsize;
m0_axi_arburst <= m_ws(0).arburst;
m0_axi_arid <= m_ws(0).arid;
m0_axi_rready <= m_ws(0).rready;
m_wis(0).awready <= m0_axi_awready;
m_wis(0).wready <= m0_axi_wready;
m_wis(0).bresp <= m0_axi_bresp;
m_wis(0).bid <= m0_axi_bid;
m_wis(0).bvalid <= m0_axi_bvalid;
m_wis(0).arready <= m0_axi_arready;
m_wis(0).rlast <= m0_axi_rlast;
m_wis(0).rresp <= m0_axi_rresp;
m_wis(0).rdata <= m0_axi_rdata;
m_wis(0).rid <= m0_axi_rid;
m_wis(0).rvalid <= m0_axi_rvalid;
m1_axi_awvalid <= m_ws(1).awvalid;
m1_axi_awaddr <= m_ws(1).awaddr;
m1_axi_awlen <= m_ws(1).awlen;
m1_axi_awsize <= m_ws(1).awsize;
m1_axi_awburst <= m_ws(1).awburst;
m1_axi_awid <= m_ws(1).awid;
m1_axi_wvalid <= m_ws(1).wvalid;
m1_axi_wdata <= m_ws(1).wdata;
m1_axi_wstrb <= m_ws(1).wstrb;
m1_axi_wlast <= m_ws(1).wlast;
m1_axi_bready <= m_ws(1).bready;
m1_axi_arvalid <= m_ws(1).arvalid;
m1_axi_araddr <= m_ws(1).araddr;
m1_axi_arlen <= m_ws(1).arlen;
m1_axi_arsize <= m_ws(1).arsize;
m1_axi_arburst <= m_ws(1).arburst;
m1_axi_arid <= m_ws(1).arid;
m1_axi_rready <= m_ws(1).rready;
m_wis(1).awready <= m1_axi_awready;
m_wis(1).wready <= m1_axi_wready;
m_wis(1).bresp <= m1_axi_bresp;
m_wis(1).bid <= m1_axi_bid;
m_wis(1).bvalid <= m1_axi_bvalid;
m_wis(1).arready <= m1_axi_arready;
m_wis(1).rlast <= m1_axi_rlast;
m_wis(1).rresp <= m1_axi_rresp;
m_wis(1).rdata <= m1_axi_rdata;
m_wis(1).rid <= m1_axi_rid;
m_wis(1).rvalid <= m1_axi_rvalid;
m2_axi_awvalid <= m_ws(2).awvalid;
m2_axi_awaddr <= m_ws(2).awaddr;
m2_axi_awlen <= m_ws(2).awlen;
m2_axi_awsize <= m_ws(2).awsize;
m2_axi_awburst <= m_ws(2).awburst;
m2_axi_awid <= m_ws(2).awid;
m2_axi_wvalid <= m_ws(2).wvalid;
m2_axi_wdata <= m_ws(2).wdata;
m2_axi_wstrb <= m_ws(2).wstrb;
m2_axi_wlast <= m_ws(2).wlast;
m2_axi_bready <= m_ws(2).bready;
m2_axi_arvalid <= m_ws(2).arvalid;
m2_axi_araddr <= m_ws(2).araddr;
m2_axi_arlen <= m_ws(2).arlen;
m2_axi_arsize <= m_ws(2).arsize;
m2_axi_arburst <= m_ws(2).arburst;
m2_axi_arid <= m_ws(2).arid;
m2_axi_rready <= m_ws(2).rready;
m_wis(2).awready <= m2_axi_awready;
m_wis(2).wready <= m2_axi_wready;
m_wis(2).bresp <= m2_axi_bresp;
m_wis(2).bid <= m2_axi_bid;
m_wis(2).bvalid <= m2_axi_bvalid;
m_wis(2).arready <= m2_axi_arready;
m_wis(2).rlast <= m2_axi_rlast;
m_wis(2).rresp <= m2_axi_rresp;
m_wis(2).rdata <= m2_axi_rdata;
m_wis(2).rid <= m2_axi_rid;
m_wis(2).rvalid <= m2_axi_rvalid;
m3_axi_awvalid <= m_ws(3).awvalid;
m3_axi_awaddr <= m_ws(3).awaddr;
m3_axi_awlen <= m_ws(3).awlen;
m3_axi_awsize <= m_ws(3).awsize;
m3_axi_awburst <= m_ws(3).awburst;
m3_axi_awid <= m_ws(3).awid;
m3_axi_wvalid <= m_ws(3).wvalid;
m3_axi_wdata <= m_ws(3).wdata;
m3_axi_wstrb <= m_ws(3).wstrb;
m3_axi_wlast <= m_ws(3).wlast;
m3_axi_bready <= m_ws(3).bready;
m3_axi_arvalid <= m_ws(3).arvalid;
m3_axi_araddr <= m_ws(3).araddr;
m3_axi_arlen <= m_ws(3).arlen;
m3_axi_arsize <= m_ws(3).arsize;
m3_axi_arburst <= m_ws(3).arburst;
m3_axi_arid <= m_ws(3).arid;
m3_axi_rready <= m_ws(3).rready;
m_wis(3).awready <= m3_axi_awready;
m_wis(3).wready <= m3_axi_wready;
m_wis(3).bresp <= m3_axi_bresp;
m_wis(3).bid <= m3_axi_bid;
m_wis(3).bvalid <= m3_axi_bvalid;
m_wis(3).arready <= m3_axi_arready;
m_wis(3).rlast <= m3_axi_rlast;
m_wis(3).rresp <= m3_axi_rresp;
m_wis(3).rdata <= m3_axi_rdata;
m_wis(3).rid <= m3_axi_rid;
m_wis(3).rvalid <= m3_axi_rvalid;

s_axi_awready <= '1' when not m_awactive and s_axi_awvalid = '1' and m_ws(get_channel(s_axi_awaddr)).awvalid = '0' and s_axi_wvalid = '1' and m_ws(get_channel(s_axi_awaddr)).wvalid = '0' else '0';
s_axi_wready <= '1' when (not m_awactive and s_axi_awvalid = '1' and m_ws(get_channel(s_axi_awaddr)).awvalid = '0' and s_axi_wvalid = '1' and m_ws(get_channel(s_axi_awaddr)).wvalid = '0') or (m_awactive and m_ws(get_channel(m_awaddr)).wvalid = '0' and m_awcounter > 0) else '0';
s_axi_arready <= '1' when not m_aractive and m_ws(get_channel(s_axi_araddr)).arvalid = '0' and m_rbusy = '0' else '0';

s_axi_rdata <= m_rbuffer_data1 when m_rbuffer_data1_valid = '1'
    else m_rbuffer_data;
s_axi_rresp <= AXI_RESP_OKAY;
s_axi_rvalid <= s_axi_rvalid_b;
s_axi_rvalid_b <= '1' when (m_rbuffer_data1_valid = '1' and m_rbuffer_data1_no = m_rsent_no) or 
        (m_rbuffer_data_valid = '1' and m_rbuffer_addr_read = m_rsent_no) else '0';
s_axi_rlast <= '1' when m_rsent_no = m_rlen else '0';
s_axi_rid <= m_arid;

m_rbuffer_raddr <= to_integer(to_unsigned(m_rsent_no_p2, 8));--to_integer(to_unsigned(m_rsent_no, 8) + 2);

m_ws(0).rready <= '1' when m_wis(0).rlast = '0' or m_ws(0).rvalid = '0' else '0';
m_ws(1).rready <= '1' when m_wis(1).rlast = '0' or m_ws(1).rvalid = '0' else '0';
m_ws(2).rready <= '1' when m_wis(2).rlast = '0' or m_ws(2).rvalid = '0' else '0';
m_ws(3).rready <= '1' when m_wis(3).rlast = '0' or m_ws(3).rvalid = '0' else '0';

i_pr : process(clk)
    variable ch, id : integer;
    variable got_data, write_data : std_logic_vector(1023 downto 0);
    variable got_no, write_no : integer;
    variable got, make_write : boolean := false;
    variable rchannel_handled : boolean;
begin
    if rising_edge(clk) then
        if resetn = '0' then
            m_rsent_no <= 0;
            m_rsent_no_p1 <= 1;
            m_rsent_no_p2 <= 2;
            m_awactive <= false;
            m_ws(0).wvalid <= '0';
            m_ws(1).wvalid <= '0';
            m_ws(2).wvalid <= '0';
            m_ws(3).wvalid <= '0';
            s_axi_bvalid <= '0';
            m_aractive <= false;
            m_rbusy <= '0';
        else
-- AW & W
            ch := get_channel(s_axi_awaddr);
            if not m_awactive and s_axi_awvalid = '1' and m_ws(ch).awvalid = '0' and s_axi_wvalid = '1' and m_ws(ch).wvalid = '0' then
                m_awactive <= true;

                m_ws(ch).awvalid <= '1';
                m_ws(ch).awaddr <= s_axi_awaddr;
                m_awaddr <= std_logic_vector(unsigned(s_axi_awaddr) + to_unsigned(128, 40));
                m_awcounter <= to_integer(unsigned(s_axi_awlen));
                m_awid <= s_axi_awid;
                m_ws(ch).awlen <= x"01";
                m_ws(ch).awburst <= AXI_BURST_INCR;
                m_ws(ch).awsize <= AXI_SIZE_64;
                m_ws(ch).awid <= m_awaddr(13 downto 10);
                
                m_ws(ch).wvalid <= '1';
                m_ws(ch).wdata <= s_axi_wdata(511 downto 0);
                m_ws(ch).wstrb <= s_axi_wstrb(63 downto 0);
                m_ws(ch).wdata2 <= s_axi_wdata(1023 downto 512);
                m_ws(ch).wstrb2 <= s_axi_wstrb(127 downto 64);

                if to_integer(unsigned(s_axi_awlen)) = 0 then 
                    s_axi_bvalid <= '1';
                    s_axi_bresp <= AXI_RESP_OKAY;
                end if;
            end if;
            ch := get_channel(m_awaddr);
            if m_awactive and s_axi_wvalid = '1' and m_ws(ch).awvalid = '0' and m_ws(ch).wvalid = '0' then
                m_ws(ch).awvalid <= '1';
                m_ws(ch).awaddr <= m_awaddr;
                m_ws(ch).awlen <= x"01";
                m_ws(ch).awburst <= AXI_BURST_INCR;
                m_ws(ch).awsize <= AXI_SIZE_64;
                m_ws(ch).awid <= m_awaddr(13 downto 10);

                m_ws(ch).wvalid <= '1';
                m_ws(ch).wdata <= s_axi_wdata(511 downto 0);
                m_ws(ch).wstrb <= s_axi_wstrb(63 downto 0);
                m_ws(ch).wdata2 <= s_axi_wdata(1023 downto 512);
                m_ws(ch).wstrb2 <= s_axi_wstrb(127 downto 64);
                
                if m_awcounter > 1 then 
                    m_awaddr <= std_logic_vector(unsigned(m_awaddr) + to_unsigned(128, 40));
                    m_awcounter <= m_awcounter - 1;
                else
                    m_awcounter <= 0;
                    s_axi_bvalid <= '1';
                    s_axi_bid <= m_awid;
                    s_axi_bresp <= AXI_RESP_OKAY;
                end if;
            end if;
            if m_awactive and m_awcounter = 0 and s_axi_bready = '1' then
                m_awactive <= false;
                s_axi_bvalid <= '0';
            end if;
                     
            if m_ws(0).awvalid = '1' and m_wis(0).awready = '1' then
                m_ws(0).awvalid <= '0';
            end if;
            for i in 0 to 3 loop
                if m_ws(i).awvalid = '1' and m_wis(i).awready = '1' then
                    m_ws(i).awvalid <= '0';
                end if;
                if m_ws(i).wvalid = '1' and m_wis(i).wready = '1' then
                    if m_ws(i).wlast = '0' then
                        m_ws(i).wdata <= m_ws(i).wdata2;
                        m_ws(i).wstrb <= m_ws(i).wstrb2;
                        m_ws(i).wlast <= '1';
                    else
                        m_ws(i).wvalid <= '0';
                        m_ws(i).wlast <= '0';
                    end if;
                end if;
            end loop;
-- AR
            ch := get_channel(s_axi_araddr);
            if not m_aractive and s_axi_arvalid = '1' and m_rbusy = '0' and m_ws(ch).arvalid = '0' then
                m_raddr <= s_axi_araddr;
                m_rlen <= to_integer(unsigned(s_axi_arlen));
                m_rbuffer_valid <= (others => '0');
                
                m_ws(ch).arvalid <= '1';
                m_ws(ch).araddr <= s_axi_araddr;
                m_araddr <= std_logic_vector(unsigned(s_axi_araddr) + to_unsigned(128, 40));
                m_arcounter <= to_integer(unsigned(s_axi_arlen));
                m_arid <= s_axi_arid;
                m_ws(ch).arlen <= x"01";
                m_ws(ch).arburst <= AXI_BURST_INCR;
                m_ws(ch).arsize <= AXI_SIZE_64;
                m_ws(ch).arid <= std_logic_vector(to_unsigned(0, 4));
                id_mask(ch)(0) <= '1';
                ids(ch, 0) <= 0;

                if to_integer(unsigned(s_axi_arlen)) /= 0 then
                    m_aractive <= true;
                    m_rid <= 1;
                end if;
                m_rbusy <= '1';
            end if;
            ch := get_channel(m_araddr);
            id := get_next_free(ch);
            if m_aractive and m_ws(ch).arvalid = '0' and id /= -1 then
                m_ws(ch).arvalid <= '1';
                m_ws(ch).araddr <= m_araddr;
                m_ws(ch).arlen <= x"01";
                m_ws(ch).arburst <= AXI_BURST_INCR;
                m_ws(ch).arsize <= AXI_SIZE_64;
                m_ws(ch).arid <= std_logic_vector(to_unsigned(id, 4));
                id_mask(ch)(id) <= '1';
                ids(ch, id) <= m_rid;
                if m_rid < m_arcounter then 
                    m_araddr <= std_logic_vector(unsigned(m_araddr) + to_unsigned(128, 40));
                    m_rid <= m_rid + 1;
                else
                    m_rid <= 0;
                    m_aractive <= false;
                end if;
            end if;
                     
            for i in 0 to 3 loop
                if m_ws(i).arvalid = '1' and m_wis(i).arready = '1' then
                    m_ws(i).arvalid <= '0';
                end if;
            end loop;
-- R
            m_rbuffer_data <= m_rbuffer(m_rbuffer_raddr);
            m_rbuffer_addr_read <= m_rbuffer_raddr;
            m_rbuffer_data_valid <= m_rbuffer_valid(m_rbuffer_raddr);
            
            make_write := false;
            for ch in 0 to 3 loop
                if m_wis(ch).rvalid = '1' and m_ws(ch).rvalid = '0' then
                    if m_wis(ch).rlast = '0' then -- first cycle
                        m_ws(ch).rdata(511 downto 0) <= m_wis(ch).rdata;
                    else -- second cycle
                        id := to_integer(unsigned(m_wis(ch).rid));
                        m_ws(ch).rid <= ids(ch, id);
                        id_mask(ch)(id) <= '0';
                        m_ws(ch).rdata(1023 downto 512) <= m_wis(ch).rdata;
                        m_ws(ch).rvalid <= '1';
                    end if;
                end if;
            end loop;
-- round-robin select
--            got := false;      
--            for i in 0 to 3 loop
--                ch := i;
--                if m_ws(ch).rvalid = '1' then
--                    got_no := m_ws(ch).rid;
--                    got_data := m_ws(ch).rdata;
--                    got := true;
--                    m_ws(ch).rvalid <= '0';
--                    exit;
--                end if;
--            end loop;
            for i in 0 to 3 loop
                if m_ws(i).rvalid = '1' then
                    incoming_no <= m_ws(i).rid;
                    incoming_data <= m_ws(i).rdata;
                    incoming_valid <= true;
                    m_ws(i).rvalid <= '0';
                    exit;
                elsif i = 3 then
                    incoming_valid <= false;
                end if;
            end loop;
            got_data := incoming_data;
            got_no := incoming_no;
            got := incoming_valid;
            if s_axi_rvalid_b = '1' and s_axi_rready = '1' then -- was sent
                if m_rsent_no = m_rlen then
                    m_rsent_no <= 0;
                    m_rsent_no_p1 <= 1;
                    m_rsent_no_p2 <= 2;
                    m_rbusy <= '0';
                else
                    m_rsent_no <= m_rsent_no + 1;
                    m_rsent_no_p1 <= m_rsent_no + 2;
                    m_rsent_no_p2 <= m_rsent_no + 3;
                end if;
                
                if m_rbuffer_data1_valid = '1' then
                    if got and m_rsent_no_p1 = got_no then
                        m_rbuffer_data1 <= got_data;
                        m_rbuffer_data1_no <= got_no;
                    elsif m_rbuffer_data2_valid = '1' and m_rbuffer_data2_no = m_rsent_no_p1 then
                        m_rbuffer_data1 <= m_rbuffer_data2;
                        m_rbuffer_data1_valid <= m_rbuffer_data2_valid;
                        m_rbuffer_data1_no <= m_rbuffer_data2_no;
                    elsif m_rbuffer_data_valid = '1' and m_rbuffer_addr_read = m_rsent_no_p1 then
                        m_rbuffer_data1 <= m_rbuffer_data;
                        m_rbuffer_data1_valid <= m_rbuffer_data_valid;
                        m_rbuffer_data1_no <= m_rbuffer_addr_read;
                    else
                        m_rbuffer_data1_valid <= '0';
                    end if;
                end if;

                if m_rbuffer_data2_valid = '1' then
                    if got and m_rsent_no_p2 = got_no then
                        m_rbuffer_data2 <= got_data;
                        m_rbuffer_data2_no <= got_no;
                    elsif m_rbuffer_data_valid = '1' and m_rbuffer_addr_read = m_rsent_no_p2 then
                        m_rbuffer_data2 <= m_rbuffer_data;
                        m_rbuffer_data2_valid <= m_rbuffer_data_valid;
                        m_rbuffer_data2_no <= m_rbuffer_addr_read;
                    else
                        m_rbuffer_data2_valid <= '0';
                    end if;
                else
                    if got and m_rsent_no_p2 = got_no then
                        m_rbuffer_data2 <= got_data;
                        m_rbuffer_data2_no <= got_no;
                        m_rbuffer_data2_valid <= '1';
                    end if;
                end if;

                if got and m_rsent_no_p2 < got_no then
                    make_write := true;
                    write_no := got_no;
                    write_data := got_data;
                end if;
            else -- no sent
                if got and m_rsent_no = got_no then
                    m_rbuffer_data1 <= got_data; -- m_rsent_no
                    m_rbuffer_data1_no <= got_no;
                    m_rbuffer_data1_valid <= '1';
                elsif m_rbuffer_data_valid = '1' and m_rbuffer_addr_read = m_rsent_no then
                    m_rbuffer_data1 <= m_rbuffer_data;
                    m_rbuffer_data1_no <= m_rbuffer_addr_read;
                    m_rbuffer_data1_valid <= m_rbuffer_data_valid;
                end if;
                if got and m_rsent_no_p1 = got_no then
                    m_rbuffer_data2 <= got_data; -- m_rsent_no + 1
                    m_rbuffer_data2_no <= got_no;
                    m_rbuffer_data2_valid <= '1';
                elsif m_rbuffer_data_valid = '1' and m_rbuffer_addr_read = m_rsent_no_p1 then
                    m_rbuffer_data2 <= m_rbuffer_data;
                    m_rbuffer_data2_no <= m_rbuffer_addr_read;
                    m_rbuffer_data2_valid <= m_rbuffer_data_valid;
                end if;
                if got and m_rsent_no_p1 < got_no then
                    make_write := true;
                    write_no := got_no;
                    write_data := got_data;
                end if;
            end if;

            if make_write then
                m_rbuffer(write_no) <= write_data;
                m_rbuffer_valid(write_no) <= '1';
            end if;
        end if;
    end if;
end process;

end Behavioral;
