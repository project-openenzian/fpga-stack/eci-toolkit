----------------------------------------------------------------------------------
-- Module Name: eci channel muxer - BD wrapper - Behavioral
----------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------
-- Multiplex ECI channels into one
-- Round robin

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity eci_channel_muxer_bd is
generic (
    CHANNELS    : integer
);
port (
    clk             : in STD_LOGIC;

    input0_header  : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data0   : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data1   : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data2   : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data3   : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data4   : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data5   : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data6   : in std_logic_vector(63 downto 0) := (others => '0');
    input0_data7   : in std_logic_vector(63 downto 0) := (others => '0');
    input0_vc_no   : in std_logic_vector(3 downto 0) := (others => '0');
    input0_size    : in std_logic_vector(2 downto 0) := (others => '0');
    input0_valid   : in std_logic := '0';
    input0_ready   : out std_logic;

    input1_header  : in std_logic_vector(63 downto 0) := (others => '0');
    input1_data0   : in std_logic_vector(63 downto 0) := (others => '0');
    input1_data1   : in std_logic_vector(63 downto 0) := (others => '0');
    input1_data2   : in std_logic_vector(63 downto 0) := (others => '0');
    input1_data3   : in std_logic_vector(63 downto 0) := (others => '0');
    input1_data4   : in std_logic_vector(63 downto 0) := (others => '0');
    input1_data5   : in std_logic_vector(63 downto 0) := (others => '0');
    input1_data6   : in std_logic_vector(63 downto 0) := (others => '0');
    input1_data7   : in std_logic_vector(63 downto 0) := (others => '0');
    input1_vc_no   : in std_logic_vector(3 downto 0) := (others => '0');
    input1_size    : in std_logic_vector(2 downto 0) := (others => '0');
    input1_valid   : in std_logic := '0';
    input1_ready   : out std_logic;

    input2_header  : in std_logic_vector(63 downto 0) := (others => '0');
    input2_data0   : in std_logic_vector(63 downto 0) := (others => '0');
    input2_data1   : in std_logic_vector(63 downto 0) := (others => '0');
    input2_data2   : in std_logic_vector(63 downto 0) := (others => '0');
    input2_data3   : in std_logic_vector(63 downto 0) := (others => '0');
    input2_data4   : in std_logic_vector(63 downto 0) := (others => '0');
    input2_data5   : in std_logic_vector(63 downto 0) := (others => '0');
    input2_data6   : in std_logic_vector(63 downto 0) := (others => '0');
    input2_data7   : in std_logic_vector(63 downto 0) := (others => '0');
    input2_vc_no   : in std_logic_vector(3 downto 0) := (others => '0');
    input2_size    : in std_logic_vector(2 downto 0) := (others => '0');
    input2_valid   : in std_logic := '0';
    input2_ready   : out std_logic;

    input3_header  : in std_logic_vector(63 downto 0) := (others => '0');
    input3_data0   : in std_logic_vector(63 downto 0) := (others => '0');
    input3_data1   : in std_logic_vector(63 downto 0) := (others => '0');
    input3_data2   : in std_logic_vector(63 downto 0) := (others => '0');
    input3_data3   : in std_logic_vector(63 downto 0) := (others => '0');
    input3_data4   : in std_logic_vector(63 downto 0) := (others => '0');
    input3_data5   : in std_logic_vector(63 downto 0) := (others => '0');
    input3_data6   : in std_logic_vector(63 downto 0) := (others => '0');
    input3_data7   : in std_logic_vector(63 downto 0) := (others => '0');
    input3_vc_no   : in std_logic_vector(3 downto 0) := (others => '0');
    input3_size    : in std_logic_vector(2 downto 0) := (others => '0');
    input3_valid   : in std_logic := '0';
    input3_ready   : out std_logic;

    output0_header  : out std_logic_vector(63 downto 0);
    output0_data0   : out std_logic_vector(63 downto 0);
    output0_data1   : out std_logic_vector(63 downto 0);
    output0_data2   : out std_logic_vector(63 downto 0);
    output0_data3   : out std_logic_vector(63 downto 0);
    output0_data4   : out std_logic_vector(63 downto 0);
    output0_data5   : out std_logic_vector(63 downto 0);
    output0_data6   : out std_logic_vector(63 downto 0);
    output0_data7   : out std_logic_vector(63 downto 0);
    output0_vc_no   : out std_logic_vector(3 downto 0);
    output0_size    : out std_logic_vector(2 downto 0);
    output0_valid   : out std_logic;
    output0_ready   : in  std_logic);
end eci_channel_muxer_bd;

architecture Behavioral of eci_channel_muxer_bd is

signal input0, input1, input2, input3, output0 : ECI_CHANNEL;

begin

output0_header <= output0.data(0);
output0_data0 <= output0.data(1);
output0_data1 <= output0.data(2);
output0_data2 <= output0.data(3);
output0_data3 <= output0.data(4);
output0_data4 <= output0.data(5);
output0_data5 <= output0.data(6);
output0_data6 <= output0.data(7);
output0_data7 <= output0.data(8);
output0_vc_no <= output0.vc_no;
output0_size <= output0.size;
output0_valid <= output0.valid;

input0.data(0) <= input0_header;
input0.data(1) <= input0_data0;
input0.data(2) <= input0_data1;
input0.data(3) <= input0_data2;
input0.data(4) <= input0_data3;
input0.data(5) <= input0_data4;
input0.data(6) <= input0_data5;
input0.data(7) <= input0_data6;
input0.data(8) <= input0_data7;
input0.vc_no <= input0_vc_no;
input0.size <= input0_size;
input0.valid <= input0_valid;

input1.data(0) <= input1_header;
input1.data(1) <= input1_data0;
input1.data(2) <= input1_data1;
input1.data(3) <= input1_data2;
input1.data(4) <= input1_data3;
input1.data(5) <= input1_data4;
input1.data(6) <= input1_data5;
input1.data(7) <= input1_data6;
input1.data(8) <= input1_data7;
input1.vc_no <= input1_vc_no;
input1.size <= input1_size;
input1.valid <= input1_valid;

input2.data(0) <= input2_header;
input2.data(1) <= input2_data0;
input2.data(2) <= input2_data1;
input2.data(3) <= input2_data2;
input2.data(4) <= input2_data3;
input2.data(5) <= input2_data4;
input2.data(6) <= input2_data5;
input2.data(7) <= input2_data6;
input2.data(8) <= input2_data7;
input2.vc_no <= input2_vc_no;
input2.size <= input2_size;
input2.valid <= input2_valid;

input3.data(0) <= input3_header;
input3.data(1) <= input3_data0;
input3.data(2) <= input3_data1;
input3.data(3) <= input3_data2;
input3.data(4) <= input3_data3;
input3.data(5) <= input3_data4;
input3.data(6) <= input3_data5;
input3.data(7) <= input3_data6;
input3.data(8) <= input3_data7;
input3.vc_no <= input3_vc_no;
input3.size <= input3_size;
input3.valid <= input3_valid;

i_muxer : entity work. eci_channel_muxer
generic map(
    CHANNELS        => 4
)
port map (
    clk             => clk,
    inputs(0)       => input0,
    inputs(1)       => input1,
    inputs(2)       => input2,
    inputs(3)       => input3,
    inputs_ready(0) => input0_ready,
    inputs_ready(1) => input1_ready,
    inputs_ready(2) => input2_ready,
    inputs_ready(3) => input3_ready,
    output          => output0,
    output_ready    => output0_ready
);

end Behavioral;
