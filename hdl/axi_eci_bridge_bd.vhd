-------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------
-- Simple AXI <-> ECI bridge, Block Design wrapper
-- Converts read requests to ECI RLDT messages
-- Converts write requests to ECI RSTT messages
-- Read responses may come reordered
-- 32-byte, 64-byte (both no burst) or 128-byte (2-beat burst) aligned access

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity axi_eci_bridge_bd is
generic (
    ID_WIDTH    : integer range 0 to 5 := 0
);
port (
    clk : in std_logic;

    eci_read_req_header : out std_logic_vector(63 downto 0);
    eci_read_req_data0  : out std_logic_vector(63 downto 0);
    eci_read_req_data1  : out std_logic_vector(63 downto 0);
    eci_read_req_data2  : out std_logic_vector(63 downto 0);
    eci_read_req_data3  : out std_logic_vector(63 downto 0);
    eci_read_req_data4  : out std_logic_vector(63 downto 0);
    eci_read_req_data5  : out std_logic_vector(63 downto 0);
    eci_read_req_data6  : out std_logic_vector(63 downto 0);
    eci_read_req_data7  : out std_logic_vector(63 downto 0);
    eci_read_req_vc_no  : out std_logic_vector(3 downto 0);
    eci_read_req_size   : out std_logic_vector(2 downto 0);
    eci_read_req_valid  : out std_logic;
    eci_read_req_ready  : in  std_logic;

    eci_write_req_header : out std_logic_vector(63 downto 0);
    eci_write_req_data0  : out std_logic_vector(63 downto 0);
    eci_write_req_data1  : out std_logic_vector(63 downto 0);
    eci_write_req_data2  : out std_logic_vector(63 downto 0);
    eci_write_req_data3  : out std_logic_vector(63 downto 0);
    eci_write_req_data4  : out std_logic_vector(63 downto 0);
    eci_write_req_data5  : out std_logic_vector(63 downto 0);
    eci_write_req_data6  : out std_logic_vector(63 downto 0);
    eci_write_req_data7  : out std_logic_vector(63 downto 0);
    eci_write_req_vc_no  : out std_logic_vector(3 downto 0);
    eci_write_req_size   : out std_logic_vector(2 downto 0);
    eci_write_req_valid  : out std_logic;
    eci_write_req_ready  : in  std_logic;

    eci_rsp_header  : in std_logic_vector(63 downto 0);
    eci_rsp_data0   : in std_logic_vector(63 downto 0);
    eci_rsp_data1   : in std_logic_vector(63 downto 0);
    eci_rsp_data2   : in std_logic_vector(63 downto 0);
    eci_rsp_data3   : in std_logic_vector(63 downto 0);
    eci_rsp_data4   : in std_logic_vector(63 downto 0);
    eci_rsp_data5   : in std_logic_vector(63 downto 0);
    eci_rsp_data6   : in std_logic_vector(63 downto 0);
    eci_rsp_data7   : in std_logic_vector(63 downto 0);
    eci_rsp_vc_no   : in std_logic_vector(3 downto 0);
    eci_rsp_size    : in std_logic_vector(2 downto 0);
    eci_rsp_valid   : in std_logic;
    eci_rsp_ready   : out std_logic;

    p_axi_araddr    : in std_logic_vector(39 downto 0);
    p_axi_arlen     : in std_logic_vector( 7 downto 0);
    p_axi_arsize    : in std_logic_vector( 2 downto 0);
    p_axi_arburst   : in std_logic_vector( 1 downto 0);
    p_axi_arid      : in std_logic_vector (ID_WIDTH-1 downto 0) := (others => '0');
    p_axi_arvalid   : in std_logic;
    p_axi_arready   : out std_logic;

    p_axi_rdata     : out std_logic_vector(511 downto 0);
    p_axi_rresp     : out std_logic_vector( 1 downto 0);
    p_axi_rlast     : out std_logic;
    p_axi_rid       : out std_logic_vector (ID_WIDTH-1 downto 0);
    p_axi_rvalid    : out std_logic;
    p_axi_rready    : in std_logic;

    p_axi_awaddr    : in std_logic_vector (39 downto 0);
    p_axi_awlen     : in std_logic_vector ( 7 downto 0);
    p_axi_awsize    : in std_logic_vector ( 2 downto 0);
    p_axi_awburst   : in std_logic_vector ( 1 downto 0);
    p_axi_awid      : in std_logic_vector (ID_WIDTH-1 downto 0) := (others => '0');
    p_axi_awvalid   : in std_logic;
    p_axi_awready   : out std_logic;

    p_axi_wdata     : in std_logic_vector (511 downto 0);
    p_axi_wstrb     : in std_logic_vector (63 downto 0);
    p_axi_wlast     : in std_logic;
    p_axi_wvalid    : in std_logic;
    p_axi_wready    : out std_logic;

    p_axi_bresp     : out std_logic_vector( 1 downto 0);
    p_axi_bid       : out std_logic_vector (ID_WIDTH-1 downto 0);
    p_axi_bvalid    : out std_logic;
    p_axi_bready    : in std_logic
);
end axi_eci_bridge_bd;

architecture behavioural of axi_eci_bridge_bd is

ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
ATTRIBUTE X_INTERFACE_PARAMETER of p_axi_araddr: SIGNAL is "CLK_DOMAIN clk,MAX_BURST_LENGTH 2,NUM_WRITE_OUTSTANDING 16,NUM_READ_OUTSTANDING 16,SUPPORTS_NARROW_BURST 0";

component axi_eci_bridge is
generic (
    ID_WIDTH    : integer range 0 to 5 := 1
);
port (
    clk : in std_logic;

    eci_read_req        : out ECI_CHANNEL;
    eci_read_req_ready  : in  std_logic;

    eci_write_req       : out ECI_CHANNEL;
    eci_write_req_ready : in  std_logic;

    eci_rsp         : in ECI_CHANNEL;
    eci_rsp_ready   : out std_logic;

    p_axi_araddr    : in std_logic_vector(39 downto 0);
    p_axi_arlen     : in std_logic_vector( 7 downto 0);
    p_axi_arsize    : in std_logic_vector( 2 downto 0);
    p_axi_arburst   : in std_logic_vector( 1 downto 0);
    p_axi_arid      : in std_logic_vector(ID_WIDTH-1 downto 0);
    p_axi_arvalid   : in std_logic;
    p_axi_arready   : out std_logic;

    p_axi_rdata     : out std_logic_vector(511 downto 0);
    p_axi_rresp     : out std_logic_vector( 1 downto 0);
    p_axi_rlast     : out std_logic;
    p_axi_rid       : out std_logic_vector(ID_WIDTH-1 downto 0);
    p_axi_rvalid    : out std_logic;
    p_axi_rready    : in std_logic;

    p_axi_awaddr    : in std_logic_vector (39 downto 0);
    p_axi_awlen     : in std_logic_vector ( 7 downto 0);
    p_axi_awsize    : in std_logic_vector ( 2 downto 0);
    p_axi_awburst   : in std_logic_vector ( 1 downto 0);
    p_axi_awid      : in std_logic_vector (ID_WIDTH-1 downto 0) := (others => '0');
    p_axi_awvalid   : in std_logic;
    p_axi_awready   : out std_logic;

    p_axi_wdata     : in std_logic_vector (511 downto 0);
    p_axi_wstrb     : in std_logic_vector (63 downto 0);
    p_axi_wlast     : in std_logic;
    p_axi_wvalid    : in std_logic;
    p_axi_wready    : out std_logic;

    p_axi_bresp     : out std_logic_vector( 1 downto 0);
    p_axi_bid       : out std_logic_vector (ID_WIDTH-1 downto 0);
    p_axi_bvalid    : out std_logic;
    p_axi_bready    : in std_logic
);
end component;

signal eci_read_req, eci_write_req, eci_rsp : ECI_CHANNEL;

begin

eci_read_req_header <= eci_read_req.data(0);
eci_read_req_data0 <= eci_read_req.data(1);
eci_read_req_data1 <= eci_read_req.data(2);
eci_read_req_data2 <= eci_read_req.data(3);
eci_read_req_data3 <= eci_read_req.data(4);
eci_read_req_data4 <= eci_read_req.data(5);
eci_read_req_data5 <= eci_read_req.data(6);
eci_read_req_data6 <= eci_read_req.data(7);
eci_read_req_data7 <= eci_read_req.data(8);
eci_read_req_vc_no <= eci_read_req.vc_no;
eci_read_req_size <= eci_read_req.size;
eci_read_req_valid <= eci_read_req.valid;

eci_write_req_header <= eci_write_req.data(0);
eci_write_req_data0 <= eci_write_req.data(1);
eci_write_req_data1 <= eci_write_req.data(2);
eci_write_req_data2 <= eci_write_req.data(3);
eci_write_req_data3 <= eci_write_req.data(4);
eci_write_req_data4 <= eci_write_req.data(5);
eci_write_req_data5 <= eci_write_req.data(6);
eci_write_req_data6 <= eci_write_req.data(7);
eci_write_req_data7 <= eci_write_req.data(8);
eci_write_req_vc_no <= eci_write_req.vc_no;
eci_write_req_size <= eci_write_req.size;
eci_write_req_valid <= eci_write_req.valid;

eci_rsp.data(0) <= eci_rsp_header;
eci_rsp.data(1) <= eci_rsp_data0;
eci_rsp.data(2) <= eci_rsp_data1;
eci_rsp.data(3) <= eci_rsp_data2;
eci_rsp.data(4) <= eci_rsp_data3;
eci_rsp.data(5) <= eci_rsp_data4;
eci_rsp.data(6) <= eci_rsp_data5;
eci_rsp.data(7) <= eci_rsp_data6;
eci_rsp.data(8) <= eci_rsp_data7;
eci_rsp.vc_no <= eci_rsp_vc_no;
eci_rsp.size <= eci_rsp_size;
eci_rsp.valid <= eci_rsp_valid;

i_bridge : axi_eci_bridge
generic map (
    ID_WIDTH        => ID_WIDTH
)
port map (
    clk => clk,

    eci_read_req        => eci_read_req,
    eci_read_req_ready  => eci_read_req_ready,

    eci_write_req       => eci_write_req,
    eci_write_req_ready => eci_write_req_ready,

    eci_rsp         => eci_rsp,
    eci_rsp_ready   => eci_rsp_ready,
    p_axi_awvalid => p_axi_awvalid,
    p_axi_awaddr => p_axi_awaddr,
    p_axi_awlen => p_axi_awlen,
    p_axi_awsize => p_axi_awsize,
    p_axi_awburst => p_axi_awburst,
    p_axi_awid => p_axi_awid,
    p_axi_wvalid => p_axi_wvalid,
    p_axi_wdata => p_axi_wdata,
    p_axi_wstrb => p_axi_wstrb,
    p_axi_wlast => p_axi_wlast,
    p_axi_bready => p_axi_bready,
    p_axi_arvalid => p_axi_arvalid,
    p_axi_araddr => p_axi_araddr,
    p_axi_arlen => p_axi_arlen,
    p_axi_arsize => p_axi_arsize,
    p_axi_arburst => p_axi_arburst,
    p_axi_arid => p_axi_arid,
    p_axi_rready => p_axi_rready,
    p_axi_awready => p_axi_awready,
    p_axi_wready => p_axi_wready,
    p_axi_bresp => p_axi_bresp,
    p_axi_bid => p_axi_bid,
    p_axi_bvalid => p_axi_bvalid,
    p_axi_arready => p_axi_arready,
    p_axi_rlast => p_axi_rlast,
    p_axi_rresp => p_axi_rresp,
    p_axi_rdata => p_axi_rdata,
    p_axi_rid => p_axi_rid,
    p_axi_rvalid => p_axi_rvalid
);

end behavioural;
