----------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------
-- ECI Channel <-> AXI Stream bridge
-- First AXI stream 32-bit word holds the size (3 bits, ECI_CHANNEL size) and the VC number (4 bits)
-- 1st AXI word: 00000000 00000000 00000000 0sssvvvv
-- 2nd AXI word: bits 31-0 of the 1st ECI word
-- 3nd AXI word: bits 63-32 of the 1st ECI word
-- and so on

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity eci_axis_bridge is
port (
    clk             : in std_logic;
    reset_n         : in std_logic;

    input_tdata     : in std_logic_vector(31 downto 0);
    input_tlast     : in std_logic;
    input_tready    : buffer std_logic;
    input_tvalid    : in std_logic;
    output          : buffer ECI_CHANNEL;
    output_ready    : in std_logic;

    input           : in ECI_CHANNEL;
    input_ready     : buffer std_logic;
    output_tdata    : buffer std_logic_vector(31 downto 0);
    output_tlast    : buffer std_logic;
    output_tready   : in std_logic;
    output_tvalid   : buffer std_logic
);
end eci_axis_bridge;

architecture Behavioral of eci_axis_bridge is

signal input_first_cycle : boolean := true;
signal output_first_cycle : boolean := true;
signal in_position  : integer := 0;
signal out_data     : WORDS(8 downto 0);
signal out_position : integer := 0;

function get_part(data : WORDS(8 downto 0); position : integer) return std_logic_vector is
    variable part : std_logic_vector(31 downto 0);
    variable d, r  : integer;
begin
    d := (position-1) / 2;
    r := (position-1) mod 2;
    part := data(d)(r * 32 + 31 downto r * 32);
    return part;
end function;

function get_final_position(size : std_logic_vector(2 downto 0)) return integer is
begin
    if size = ECI_CHANNEL_SIZE_0L then
        return 2;
    elsif size = ECI_CHANNEL_SIZE_1F and size = ECI_CHANNEL_SIZE_1L then
        return 4;
    elsif size = ECI_CHANNEL_SIZE_4F or size = ECI_CHANNEL_SIZE_4L then
        return 10;
    else -- size = ECI_CHANNEL_SIZE_8F or size = ECI_CHANNEL_SIZE_8L
        return 18;
    end if;
end function;

begin

gen_out_data : for i in 0 to 8 generate
    output.data(i)(31 downto 0) <= out_data(i)(31 downto 0);
    output.data(i)(63 downto 32) <= input_tdata when in_position = get_final_position(output.size) and i = (in_position-1) / 2 else out_data(i)(63 downto 32);
end generate;

output.valid <= to_std_logic(in_position = get_final_position(output.size)) and input_tvalid;
input_tready <= output_ready when in_position = get_final_position(output.size) else '1';

output_tdata <= x"000000" & "0" & input.size & input.vc_no when out_position = 0 else
        get_part(input.data, out_position);
output_tvalid <= input.valid;
output_tlast <= to_std_logic(out_position = get_final_position(input.size));
input_ready <= output_tready when out_position = get_final_position(input.size) else '0';

i_process : process(clk)
    variable d, r   : integer;
begin
    if rising_edge(clk) then
        if reset_n = '1' then
            if input_tvalid = '1' then
                if in_position = 0 then
                    output.vc_no <= input_tdata(3 downto 0);
                    output.size <= input_tdata(6 downto 4);
                    if output_first_cycle then -- skip the header in the 2nd cycle
                        in_position <= 1;
                    else
                        in_position <= 3;
                    end if;
                elsif in_position /= get_final_position(output.size) then
                    d := (in_position-1) / 2;
                    r := (in_position-1) mod 2;
                    out_data(d)(r * 32 + 31 downto r * 32) <= input_tdata;
                    in_position <= in_position + 1;
                elsif output_ready = '1' then
                    in_position <= 0;
                end if;
                if input_tready = '1' then
                    if is_eci_channel_cycle_last(output.size) then
                        output_first_cycle <= true;
                    else
                        output_first_cycle <= false;
                    end if;
                end if;
            end if;
            if input.valid = '1' and output_tready = '1' then
                if out_position = get_final_position(input.size) then
                    if is_eci_channel_cycle_last(input.size) then
                        out_position <= 0;
                    else
                        out_position <= 3;
                    end if;
                else
                    out_position <= out_position + 1;
                end if;
                if is_eci_channel_cycle_last(input.size) then
                    input_first_cycle <= true;
                else
                    input_first_cycle <= false;
                end if;
            end if;
        else
           in_position <= 0;
           out_position <= 0;
        end if;
    end if;
end process;

end Behavioral;
