-------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------
-- Simple DMA engine
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity axi_dma_engine_bd is
generic (
    MAX_BURST_LEN : integer range 1 to 256 := 256;
    ADDRESS_WIDTH : integer range 1 to 63 := 40
);
port (
    clk : in std_logic;
    resetn : in std_logic;

-- app AXI lite
    s_axi_awaddr : in std_logic_vector(39 downto 0);
    s_axi_awvalid : in std_logic;
    s_axi_awready : out std_logic;

    s_axi_wdata : in std_logic_vector(63 downto 0);
    s_axi_wstrb : in std_logic_vector(7 downto 0);
    s_axi_wvalid : in std_logic;
    s_axi_wready : out std_logic;

    s_axi_bready : in std_logic;
    s_axi_bresp : out std_logic_vector(1 downto 0);
    s_axi_bvalid : out std_logic;

    s_axi_araddr : in std_logic_vector(39 downto 0);
    s_axi_arvalid : in std_logic;
    s_axi_arready : out std_logic;

    s_axi_rready : in std_logic;
    s_axi_rvalid : out std_logic;
    s_axi_rresp : out std_logic_vector(1 downto 0);
    s_axi_rdata : out std_logic_vector(63 downto 0);
-- master AXI
    m_axi_awaddr    : out std_logic_vector(39 downto 0);
    m_axi_awlen     : out std_logic_vector(7 downto 0);
    m_axi_awsize    : out std_logic_vector(2 downto 0);
    m_axi_awburst   : out std_logic_vector(1 downto 0);
    m_axi_awvalid   : out std_logic;
    m_axi_awready   : in std_logic;

    m_axi_wdata : out std_logic_vector(1023 downto 0);
    m_axi_wstrb : out std_logic_vector(127 downto 0);
    m_axi_wlast : out std_logic;
    m_axi_wvalid : out std_logic;
    m_axi_wready : in std_logic;

    m_axi_bready    : out std_logic;
    m_axi_bresp     : in std_logic_vector(1 downto 0);
    m_axi_bvalid    : in std_logic;

    m_axi_araddr    : out std_logic_vector(39 downto 0);
    m_axi_arlen     : out std_logic_vector(7 downto 0);
    m_axi_arsize    : out std_logic_vector(2 downto 0);
    m_axi_arburst   : out std_logic_vector(1 downto 0);
    m_axi_arvalid   : out std_logic;
    m_axi_arready   : in std_logic;

    m_axi_rready    : out std_logic;
    m_axi_rlast     : in std_logic;
    m_axi_rvalid    : in std_logic;
    m_axi_rresp     : in std_logic_vector(1 downto 0);
    m_axi_rdata     : in std_logic_vector(1023 downto 0)
);
end axi_dma_engine_bd;

architecture behavioural of axi_dma_engine_bd is

ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
ATTRIBUTE X_INTERFACE_PARAMETER of m_axi_araddr: SIGNAL is "CLK_DOMAIN clk,MAX_BURST_LENGTH 256,NUM_WRITE_OUTSTANDING 1,NUM_READ_OUTSTANDING 1,SUPPORTS_NARROW_BURST 0, ID_WIDTH 0";

signal src_addr : unsigned(39 downto 0) := (others => '0');
signal dst_addr : unsigned(39 downto 0) := (others => '0');
signal current_pos, len : unsigned(23 downto 0) := (others => '0');
signal m_axi_arvalid_b, m_axi_awvalid_b, s_axi_rvalid_b, s_axi_bvalid_b : std_logic := '0';
signal m_axi_len_b : unsigned(7 downto 0) := x"00";

signal active : std_logic := '0';

begin

active <= or_reduce(std_logic_vector(len));

s_axi_awready <= not active;

s_axi_wready <= not active;

s_axi_arready <= not s_axi_rvalid_b;

s_axi_rvalid <= s_axi_rvalid_b;

s_axi_bvalid <= s_axi_bvalid_b;
s_axi_bresp <= AXI_RESP_OKAY;


m_axi_wdata <= m_axi_rdata;
m_axi_wstrb <= (others => '1');
m_axi_wvalid <= m_axi_rvalid;
m_axi_wlast <= m_axi_rlast;
m_axi_rready <= m_axi_wready;

m_axi_araddr <= std_logic_vector(src_addr);
m_axi_arlen <= std_logic_vector(m_axi_len_b);
m_axi_arsize <= AXI_SIZE_128;
m_axi_arburst <= AXI_BURST_INCR;
m_axi_arvalid <= m_axi_arvalid_b;

m_axi_awaddr <= std_logic_vector(dst_addr);
m_axi_awlen <= std_logic_vector(m_axi_len_b);
m_axi_awsize <= AXI_SIZE_128;
m_axi_awburst <= AXI_BURST_INCR;
m_axi_awvalid <= m_axi_awvalid_b;

m_axi_bready <= '1';

i_process : process(clk)
    variable s, d : unsigned(39 downto 0);
    variable l, np : unsigned(23 downto 0);
begin
    if rising_edge(clk) then
        if resetn = '0' then
            src_addr <= (others => '0');
            dst_addr <= (others => '0');
            len <= (others => '0');
            current_pos <= (others => '0');
        else
            if active = '0' and s_axi_awvalid = '1' and s_axi_wvalid = '1' then -- initiate the transfer, write the source address and the length
                l := unsigned(s_axi_wdata(63 downto 40));
                s := unsigned(s_axi_wdata(39 downto 0));
                d := unsigned(s_axi_awaddr);
                len <= l;
                src_addr <= s;
                dst_addr <= d;
                current_pos <= to_unsigned(0, 24);
                m_axi_arvalid_b <= '1';
                m_axi_awvalid_b <= '1';
                s_axi_bvalid_b <= '1';
                if l > MAX_BURST_LEN * 128 then
                    m_axi_len_b <= to_unsigned(MAX_BURST_LEN - 1, 8);
                else
                    l := unsigned(s_axi_wdata(63 downto 40)) / 128 - 1;
                    m_axi_len_b <= l(7 downto 0);
                end if;
            end if;
            if m_axi_arvalid_b = '1' and m_axi_arready = '1' then
                m_axi_arvalid_b <= '0';
            end if;
            if m_axi_awvalid_b = '1' and m_axi_awready = '1' then
                m_axi_awvalid_b <= '0';
            end if;
            if s_axi_bvalid_b = '1' and s_axi_bready = '1' then
                s_axi_bvalid_b <= '0';
            end if;
            if active = '1' and m_axi_rvalid = '1' and m_axi_wready = '1' and m_axi_rlast = '1' then -- last beat
                np := (x"0" & m_axi_len_b + 1) * 128;
                src_addr <= src_addr + np;
                dst_addr <= dst_addr + np;
                np := current_pos + np;
                current_pos <= np;
                if np = len then -- done
                    len <= (others => '0');
                else
                    m_axi_arvalid_b <= '1';
                    m_axi_awvalid_b <= '1';
                    if len - np > MAX_BURST_LEN * 128 then
                        m_axi_len_b <= to_unsigned(MAX_BURST_LEN - 1, 8);
                    else
                        l := (len - np) / 128 - 1;
                        m_axi_len_b <= l(7 downto 0);
                    end if;
                end if;
            end if;
            if s_axi_arvalid = '1' then -- read the status of the transfer
                s_axi_rvalid_b <= '1';
                s_axi_rdata(63 downto 24) <= (others => '0');
                s_axi_rdata(23 downto 0) <= std_logic_vector(current_pos);
                s_axi_rresp <= AXI_RESP_OKAY;
            elsif s_axi_rvalid_b = '1' and s_axi_rready = '1' then
                s_axi_rvalid_b <= '0';
            end if;
        end if;
    end if;
end process;

end behavioural;
